package v1

import (
	"gf-admin/utility/response"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

type PostSearchReq struct {
	g.Meta   `path:"/post/list" tags:"岗位管理" method:"get" summary:"岗位列表"`
	PostCode string `p:"postCode"` //岗位编码
	PostName string `p:"postName"` //岗位名称
	Status   string `p:"status"`   //状态
	response.PageReq
}

type PostSearchRes struct {
	g.Meta `mime:"application/json"`
	response.ListRes
	Content []*SysPost `json:"content"`
}

type SysPost struct {
	PostId    uint64      `json:"postId"    dc:"岗位ID"`
	PostCode  string      `json:"postCode"  dc:"岗位编码"`
	PostName  string      `json:"postName"  dc:"岗位名称"`
	PostSort  int         `json:"postSort"  dc:"显示顺序"`
	Status    uint        `json:"status"    dc:"状态（0正常 1停用）"`
	Remark    string      `json:"remark"    dc:"备注"`
	CreatedBy uint64      `json:"createdBy" dc:"创建人"`
	UpdatedBy uint64      `json:"updatedBy" dc:"修改人"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改时间"`
	DeletedAt *gtime.Time `json:"deletedAt" dc:"删除时间"`
}

type PostAddReq struct {
	g.Meta   `path:"/post/add" tags:"岗位管理" method:"post" summary:"添加岗位"`
	PostCode string `p:"postCode" v:"required#岗位编码不能为空" dc:"岗位编码"`
	PostName string `p:"postName" v:"required#岗位名称不能为空" dc:"岗位名称"`
	PostSort int    `p:"postSort" v:"required#岗位排序不能为空" dc:"显示顺序"`
	Status   uint   `p:"status" v:"required#状态不能为空" dc:"状态（0正常 1停用）"`
	Remark   string `p:"remark" dc:"备注"`
}

type PostAddRes struct {
}

type PostDeleteReq struct {
	g.Meta `path:"/post/delete" tags:"岗位管理" method:"delete" summary:"删除岗位"`
	Ids    []int `p:"ids"`
}

type PostDeleteRes struct {
}

type PostEditReq struct {
	g.Meta   `path:"/post/edit" tags:"岗位管理" method:"put" summary:"修改岗位"`
	PostId   int64  `p:"postId" v:"required#id必须"`
	PostCode string `p:"postCode" v:"required#岗位编码不能为空"`
	PostName string `p:"postName" v:"required#岗位名称不能为空"`
	PostSort int    `p:"postSort" v:"required#岗位排序不能为空"`
	Status   uint   `p:"status" v:"required#状态不能为空"`
	Remark   string `p:"remark"`
}

type PostEditRes struct {
}
