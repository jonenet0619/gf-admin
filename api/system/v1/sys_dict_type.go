package v1

import (
	"gf-admin/utility/response"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

type DictTypeSearchReq struct {
	g.Meta   `path:"/dict/type/list" tags:"字典管理" method:"get" summary:"字典类型列表"`
	DictName string `p:"dictName"` //字典名称
	DictType string `p:"dictType"` //字典类型
	Status   string `p:"status"`   //字典状态
	response.PageReq
}

type DictTypeSearchRes struct {
	g.Meta  `mime:"application/json"`
	Content []*SysDictTypeInfoRes `json:"content"`
	response.ListRes
}

type SysDictTypeInfoRes struct {
	DictId    uint64      `orm:"dict_id,primary"  json:"dictId" dc:"字典主键"`        // 字典主键
	DictName  string      `orm:"dict_name"        json:"dictName" dc:"字典名称"`      // 字典名称
	DictType  string      `orm:"dict_type,unique" json:"dictType" dc:"字典类型"`      // 字典类型
	Status    uint        `orm:"status"           json:"status" dc:"状态（0正常 1停用）"` // 状态（0正常 1停用）
	Remark    string      `orm:"remark"           json:"remark" dc:"备注"`          // 备注
	CreatedAt *gtime.Time `orm:"created_at"       json:"createdAt" dc:"创建日期"`     // 创建日期
}

type DictTypeAddReq struct {
	g.Meta   `path:"/dict/type/add" tags:"字典管理" method:"post" summary:"添加字典类型"`
	DictName string `p:"dictName"  v:"required#字典名称不能为空"`
	DictType string `p:"dictType"  v:"required#字典类型不能为空"`
	Status   uint   `p:"status"  v:"required|in:0,1#状态不能为空|状态只能为0或1"`
	Remark   string `p:"remark"`
}

type DictTypeAddRes struct {
}

type DictTypeDeleteReq struct {
	g.Meta  `path:"/dict/type/delete" tags:"字典管理" method:"delete" summary:"删除字典类型"`
	DictIds []int `p:"dictIds" v:"required#字典类型id不能为空"`
}

type DictTypeDeleteRes struct {
}

// 编辑字典
type DictTypeGetReq struct {
	g.Meta `path:"/dict/type/get" tags:"字典管理" method:"get" summary:"获取字典类型"`
	DictId uint `p:"dictId" v:"required#类型id不能为空"`
}

type DictTypeGetRes struct {
	g.Meta  `mime:"application/json"`
	Content *SysDictType `json:"content"`
}

type SysDictType struct {
	DictId    uint64      `json:"dictId"    dc:"字典主键"`
	DictName  string      `json:"dictName"  dc:"字典名称"`
	DictType  string      `json:"dictType"  dc:"字典类型"`
	Status    uint        `json:"status"    dc:"状态（0正常 1停用）"`
	CreateBy  uint        `json:"createBy"  dc:"创建者"`
	UpdateBy  uint        `json:"updateBy"  dc:"更新者"`
	Remark    string      `json:"remark"    dc:"备注"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建日期"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改日期"`
}

type DictTypeEditReq struct {
	g.Meta   `path:"/dict/type/edit" tags:"字典管理" method:"put" summary:"修改字典类型"`
	DictId   int64  `p:"dictId" v:"required|min:1#主键ID不能为空|主键ID必须为大于0的值"`
	DictName string `p:"dictName"  v:"required#字典名称不能为空"`
	DictType string `p:"dictType"  v:"required#字典类型不能为空"`
	Status   uint   `p:"status"  v:"required|in:0,1#状态不能为空|状态只能为0或1"`
	Remark   string `p:"remark"`
}

type DictTypeEditRes struct {
}
