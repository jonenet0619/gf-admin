package v1

import "github.com/gogf/gf/v2/frame/g"

type CaptchaReq struct {
	g.Meta `path:"/captcha" method:"get" tags:"用户相关" sm:"图片验证码"`
}

type CaptchaRes struct {
	Content *Captcha `json:"content"`
}

type Captcha struct {
	Id        string `json:"id"`
	RequestId string `json:"requestId"`
	Data      string `json:"data"`
}
