package v1

import (
	"gf-admin/internal/model"
	"gf-admin/utility/response"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// GetDictReq 获取字典信息请求参数
type GetDictReq struct {
	g.Meta `path:"/dict/data/getDictData" tags:"字典管理" method:"get" summary:"获取字典数据公共方法"`
	response.Author
	DictType     string `p:"dictType" v:"required#字典类型不能为空"`
	DefaultValue string `p:"defaultValue"`
}

// GetDictRes 完整的一个字典信息
type GetDictRes struct {
	g.Meta  `mime:"application/json"`
	Content *Dict `json:"content"`
}

type Dict struct {
	Info   *model.DictTypeRes   `json:"info"`
	Values []*model.DictDataRes `json:"values"`
}

// DictDataSearchReq 分页请求参数
type DictDataSearchReq struct {
	g.Meta    `path:"/dict/data/list" tags:"字典管理" method:"get" summary:"字典数据列表"`
	DictType  string `p:"dictType"`  //字典类型
	DictLabel string `p:"dictLabel"` //字典标签
	Status    string `p:"status"`    //状态
	response.PageReq
}

// DictDataSearchRes 字典数据结果
type DictDataSearchRes struct {
	g.Meta  `mime:"application/json"`
	Content []*SysDictData `json:"content"`
	response.ListRes
}

type SysDictData struct {
	DictCode  int64       `json:"dictCode"  dc:"字典编码"`
	DictSort  int         `json:"dictSort"  dc:"字典排序"`
	DictLabel string      `json:"dictLabel" dc:"字典标签"`
	DictValue string      `json:"dictValue" dc:"字典键值"`
	DictType  string      `json:"dictType"  dc:"字典类型"`
	CssClass  string      `json:"cssClass"  dc:"样式属性（其他样式扩展）"`
	ListClass string      `json:"listClass" dc:"表格回显样式"`
	IsDefault int         `json:"isDefault" dc:"是否默认（1是 0否）"`
	Status    int         `json:"status"    dc:"状态（0正常 1停用）"`
	CreateBy  uint64      `json:"createBy"  dc:"创建者"`
	UpdateBy  uint64      `json:"updateBy"  dc:"更新者"`
	Remark    string      `json:"remark"    dc:"备注"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改时间"`
}

type DictDataReq struct {
	DictLabel string `p:"dictLabel"  v:"required#字典标签不能为空"`
	DictValue string `p:"dictValue"  v:"required#字典键值不能为空"`
	DictType  string `p:"dictType"  v:"required#字典类型不能为空"`
	DictSort  int    `p:"dictSort"  v:"integer#排序只能为整数"`
	CssClass  string `p:"cssClass"`
	ListClass string `p:"listClass"`
	IsDefault int    `p:"isDefault" v:"required|in:0,1#系统默认不能为空|默认值只能为0或1"`
	Status    int    `p:"status"    v:"required|in:0,1#状态不能为空|状态只能为0或1"`
	Remark    string `p:"remark"`
}

type DictDataAddReq struct {
	g.Meta `path:"/dict/data/add" tags:"字典管理" method:"post" summary:"添加字典数据"`
	*DictDataReq
}

type DictDataAddRes struct {
}

//删除字典数据
type DictDataDeleteReq struct {
	g.Meta `path:"/dict/data/delete" tags:"字典管理" method:"delete" summary:"删除字典数据"`
	Ids    []int `p:"ids"`
}

type DictDataDeleteRes struct {
}

//获取单条字典数据
type DictDataGetReq struct {
	g.Meta   `path:"/dict/data/get" tags:"字典管理" method:"get" summary:"获取字典数据"`
	DictCode uint `p:"dictCode"`
}

type DictDataGetRes struct {
	g.Meta `mime:"application/json"`
	Dict   *SysDictData `json:"dict"`
}

//修改字典数据
type DictDataEditReq struct {
	g.Meta   `path:"/dict/data/edit" tags:"字典管理" method:"put" summary:"修改字典数据"`
	DictCode int `p:"dictCode" v:"required|min:1#主键ID不能为空|主键ID不能小于1"`
	*DictDataReq
}

type DictDataEditRes struct {
}
