package v1

import (
	"gf-admin/internal/model"
	"github.com/gogf/gf/v2/frame/g"
)

type DeptSearchReq struct {
	g.Meta   `path:"/dept/list" tags:"部门管理" method:"get" summary:"部门列表"`
	DeptName string `p:"deptName"`
	Status   string `p:"status"`
	IsType   int    `json:"isType" dc:"是否获取树状结构: 0默认是获取树状结构,1是获取列表"`
}

type DeptSearchRes struct {
	Content []*model.SysDept `json:"content"`
}

type DeptDeleteReq struct {
	g.Meta `path:"/dept/delete" tags:"部门管理" method:"delete" summary:"删除部门"`
	Id     int64 `p:"id" v:"required#id不能为空"`
}

type DeptDeleteRes struct {
}

type DeptEditReq struct {
	g.Meta   `path:"/dept/edit" tags:"部门管理" method:"put" summary:"修改部门"`
	DeptId   int    `p:"deptId" v:"required#deptId不能为空"`
	ParentID int    `p:"parentId"  v:"required#父级不能为空"`
	DeptName string `p:"deptName"  v:"required#部门名称不能为空"`
	OrderNum int    `p:"orderNum"  v:"required#排序不能为空"`
	Leader   string `p:"leader"`
	Phone    string `p:"phone"`
	Email    string `p:"email"  v:"email#邮箱格式不正确"`
	Status   uint   `p:"status"  v:"required#状态必须"`
}

type DeptEditRes struct {
}

type DeptAddReq struct {
	g.Meta   `path:"/dept/add" tags:"部门管理" method:"post" summary:"添加部门"`
	ParentID int    `p:"parentId"  v:"required#父级不能为空"`
	DeptName string `p:"deptName"  v:"required#部门名称不能为空"`
	OrderNum int    `p:"orderNum"  v:"required#排序不能为空"`
	Leader   string `p:"leader"`
	Phone    string `p:"phone"`
	Email    string `p:"email"  v:"email#邮箱格式不正确"`
	Status   uint   `p:"status"  v:"required#状态必须"`
}

type DeptAddRes struct {
}
