package v1

import (
	"gf-admin/internal/model"
	"gf-admin/utility/response"
	"github.com/gogf/gf/v2/frame/g"
)

type SysUserReq struct {
	g.Meta `path:"/getUserInfo" method:"get" tags:"用户相关" sm:"获取用户信息"`
}

type SysUserRes struct {
	Content *SysUser `json:"content"`
}

type SysUser struct {
	Permissions []string            `json:"permissions" dc:"权限"`
	UserInfo    *model.LoginUserRes `json:"userInfo"`
	MenuList    []*model.SysMenu    `json:"menuList"`
}

type UserDeptTreeSelectReq struct {
	g.Meta `path:"/dept/treeSelect" tags:"部门管理" method:"get" summary:"获取部门树形菜单"`
}

type UserDeptTreeSelectRes struct {
	g.Meta  `mime:"application/json"`
	Content []*model.SysDept `json:"content"`
}

// UserSearchReq 用户搜索请求参数
type UserSearchReq struct {
	g.Meta   `path:"/user/list" tags:"用户管理" method:"get" summary:"用户列表"`
	DeptId   string `p:"deptId"` //部门id
	Mobile   string `p:"mobile"`
	Status   string `p:"status"`
	KeyWords string `p:"keyWords"`
	response.PageReq
	response.Author
}

type UserSearchRes struct {
	g.Meta  `mime:"application/json"`
	Content []*model.SysUserRoleDeptRes `json:"content"`
	response.ListRes
}

type UserDeleteReq struct {
	g.Meta `path:"/user/delete" tags:"用户管理" method:"delete" summary:"删除用户"`
	Ids    []int `p:"ids"  v:"required#ids不能为空"`
}

type UserDeleteRes struct {
}

type UserGetEditReq struct {
	g.Meta `path:"/user/getEdit" tags:"用户管理" method:"get" summary:"获取用户信息"`
	Id     uint64 `p:"id"`
}

type UserGetEditRes struct {
	g.Meta  `mime:"application/json"`
	Content UserGetEdit `json:"content"`
}

type UserGetEdit struct {
	User           *model.SysUser `json:"user"`
	CheckedRoleIds []uint         `json:"checkedRoleIds"`
	CheckedPosts   []int64        `json:"checkedPosts"`
}

type UserGetParamsReq struct {
	g.Meta `path:"/user/params" tags:"用户管理" method:"get" summary:"用户维护参数获取"`
}

type UserGetParamsRes struct {
	g.Meta  `mime:"application/json"`
	Content *UserGetParams `json:"content"`
}

type UserGetParams struct {
	RoleList []*model.SysRole `json:"roleList"`
	Posts    []*model.SysPost `json:"posts"`
}

type UserEditReq struct {
	g.Meta `path:"/user/edit" tags:"用户管理" method:"put" summary:"修改用户"`
	*SetUserReq
	UserId int64 `p:"userId" v:"required#用户id不能为空"`
}

type UserEditRes struct {
}

type SetUserReq struct {
	DeptId   uint64  `p:"deptId" v:"required#用户部门不能为空"` //所属部门
	Email    string  `p:"email" v:"email#邮箱格式错误"`       //邮箱
	NickName string  `p:"nickName" v:"required#用户昵称不能为空"`
	Mobile   string  `p:"mobile" v:"required|phone#手机号不能为空|手机号格式错误"`
	PostIds  []int64 `p:"postIds"`
	Remark   string  `p:"remark"`
	RoleIds  []int64 `p:"roleIds"`
	Sex      int     `p:"sex"`
	Status   uint    `p:"status"`
	IsAdmin  int     `p:"isAdmin"` // 是否后台管理员 1 是  0   否
}

// UserAddReq 添加用户参数
type UserAddReq struct {
	g.Meta `path:"/user/add" tags:"用户管理" method:"post" summary:"添加用户"`
	*SetUserReq
	UserName string `p:"userName" v:"required#用户账号不能为空"`
	Password string `p:"password" v:"required|password#密码不能为空|密码以字母开头，只能包含字母、数字和下划线，长度在6~18之间"`
	UserSalt string
}

type UserAddRes struct {
}

// UserResetPwdReq 重置用户密码状态参数
type UserResetPwdReq struct {
	g.Meta   `path:"/user/resetPwd" tags:"用户管理" method:"put" summary:"重置用户密码"`
	Id       uint64 `p:"userId" v:"required#用户id不能为空"`
	Password string `p:"password" v:"required|password#密码不能为空|密码以字母开头，只能包含字母、数字和下划线，长度在6~18之间"`
}

type UserResetPwdRes struct {
}
