// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package system

import (
	"context"

	"gf-admin/api/system/v1"
)

type ISystemV1 interface {
	Captcha(ctx context.Context, req *v1.CaptchaReq) (res *v1.CaptchaRes, err error)
	DeptSearch(ctx context.Context, req *v1.DeptSearchReq) (res *v1.DeptSearchRes, err error)
	DeptDelete(ctx context.Context, req *v1.DeptDeleteReq) (res *v1.DeptDeleteRes, err error)
	DeptEdit(ctx context.Context, req *v1.DeptEditReq) (res *v1.DeptEditRes, err error)
	DeptAdd(ctx context.Context, req *v1.DeptAddReq) (res *v1.DeptAddRes, err error)
	GetDict(ctx context.Context, req *v1.GetDictReq) (res *v1.GetDictRes, err error)
	DictDataSearch(ctx context.Context, req *v1.DictDataSearchReq) (res *v1.DictDataSearchRes, err error)
	DictDataAdd(ctx context.Context, req *v1.DictDataAddReq) (res *v1.DictDataAddRes, err error)
	DictDataDelete(ctx context.Context, req *v1.DictDataDeleteReq) (res *v1.DictDataDeleteRes, err error)
	DictDataGet(ctx context.Context, req *v1.DictDataGetReq) (res *v1.DictDataGetRes, err error)
	DictDataEdit(ctx context.Context, req *v1.DictDataEditReq) (res *v1.DictDataEditRes, err error)
	DictTypeSearch(ctx context.Context, req *v1.DictTypeSearchReq) (res *v1.DictTypeSearchRes, err error)
	DictTypeAdd(ctx context.Context, req *v1.DictTypeAddReq) (res *v1.DictTypeAddRes, err error)
	DictTypeDelete(ctx context.Context, req *v1.DictTypeDeleteReq) (res *v1.DictTypeDeleteRes, err error)
	DictTypeGet(ctx context.Context, req *v1.DictTypeGetReq) (res *v1.DictTypeGetRes, err error)
	DictTypeEdit(ctx context.Context, req *v1.DictTypeEditReq) (res *v1.DictTypeEditRes, err error)
	SysMenus(ctx context.Context, req *v1.SysMenusReq) (res *v1.SysMenusRes, err error)
	SysMenusData(ctx context.Context, req *v1.SysMenusDataReq) (res *v1.SysMenusDataRes, err error)
	RuleGetParams(ctx context.Context, req *v1.RuleGetParamsReq) (res *v1.RuleGetParamsRes, err error)
	RuleInfo(ctx context.Context, req *v1.RuleInfoReq) (res *v1.RuleInfoRes, err error)
	RuleAdd(ctx context.Context, req *v1.RuleAddReq) (res *v1.RuleAddRes, err error)
	RuleDelete(ctx context.Context, req *v1.RuleDeleteReq) (res *v1.RuleDeleteRes, err error)
	RuleUpdate(ctx context.Context, req *v1.RuleUpdateReq) (res *v1.RuleUpdateRes, err error)
	PostSearch(ctx context.Context, req *v1.PostSearchReq) (res *v1.PostSearchRes, err error)
	PostAdd(ctx context.Context, req *v1.PostAddReq) (res *v1.PostAddRes, err error)
	PostDelete(ctx context.Context, req *v1.PostDeleteReq) (res *v1.PostDeleteRes, err error)
	PostEdit(ctx context.Context, req *v1.PostEditReq) (res *v1.PostEditRes, err error)
	RoleList(ctx context.Context, req *v1.RoleListReq) (res *v1.RoleListRes, err error)
	RoleGetParams(ctx context.Context, req *v1.RoleGetParamsReq) (res *v1.RoleGetParamsRes, err error)
	RoleDelete(ctx context.Context, req *v1.RoleDeleteReq) (res *v1.RoleDeleteRes, err error)
	RoleGet(ctx context.Context, req *v1.RoleGetReq) (res *v1.RoleGetRes, err error)
	RoleEdit(ctx context.Context, req *v1.RoleEditReq) (res *v1.RoleEditRes, err error)
	RoleAdd(ctx context.Context, req *v1.RoleAddReq) (res *v1.RoleAddRes, err error)
	SysUser(ctx context.Context, req *v1.SysUserReq) (res *v1.SysUserRes, err error)
	UserDeptTreeSelect(ctx context.Context, req *v1.UserDeptTreeSelectReq) (res *v1.UserDeptTreeSelectRes, err error)
	UserSearch(ctx context.Context, req *v1.UserSearchReq) (res *v1.UserSearchRes, err error)
	UserDelete(ctx context.Context, req *v1.UserDeleteReq) (res *v1.UserDeleteRes, err error)
	UserGetEdit(ctx context.Context, req *v1.UserGetEditReq) (res *v1.UserGetEditRes, err error)
	UserGetParams(ctx context.Context, req *v1.UserGetParamsReq) (res *v1.UserGetParamsRes, err error)
	UserEdit(ctx context.Context, req *v1.UserEditReq) (res *v1.UserEditRes, err error)
	UserAdd(ctx context.Context, req *v1.UserAddReq) (res *v1.UserAddRes, err error)
	UserResetPwd(ctx context.Context, req *v1.UserResetPwdReq) (res *v1.UserResetPwdRes, err error)
}
