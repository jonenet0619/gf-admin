package main

import (
	"gf-admin/internal/cmd"
	_ "gf-admin/internal/logic"
	_ "gf-admin/internal/packed"
	_ "github.com/gogf/gf/contrib/drivers/mysql/v2"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/os/gctx"
)

func main() {

	cmd.Main.Run(gctx.GetInitCtx())
}
