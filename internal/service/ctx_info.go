// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	"gf-admin/internal/model/entity"
)

type (
	ICtxInfo interface {
		GetUserInfo(ctx context.Context) (err error, user *entity.SysUser)
	}
)

var (
	localCtxInfo ICtxInfo
)

func CtxInfo() ICtxInfo {
	if localCtxInfo == nil {
		panic("implement not found for interface ICtxInfo, forgot register?")
	}
	return localCtxInfo
}

func RegisterCtxInfo(i ICtxInfo) {
	localCtxInfo = i
}
