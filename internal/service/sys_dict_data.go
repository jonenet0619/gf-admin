// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
)

type (
	ISysDictData interface {
		//获取字典数据列表
		List(ctx context.Context, req *v1.DictDataSearchReq) (res *v1.DictDataSearchRes, err error)
		//新增字典数据
		Add(ctx context.Context, req *v1.DictDataAddReq, userId uint64) (errs error)
		//删除字典数据
		Delete(ctx context.Context, ids []int) (errs error)
		//获取单条字典数据
		Get(ctx context.Context, dictCode uint) (res *v1.DictDataGetRes, errs error)
		//修改字典数据
		Edit(ctx context.Context, req *v1.DictDataEditReq, userId uint64) (errs error)
		//这条接口就是给外部调用字典的
		GetDictWithDataByType(ctx context.Context, req *v1.GetDictReq) (dict *v1.GetDictRes, err error)
	}
)

var (
	localSysDictData ISysDictData
)

func SysDictData() ISysDictData {
	if localSysDictData == nil {
		panic("implement not found for interface ISysDictData, forgot register?")
	}
	return localSysDictData
}

func RegisterSysDictData(i ISysDictData) {
	localSysDictData = i
}
