// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model"
)

type (
	ISysPost interface {
		// 岗位列表
		List(ctx context.Context, req *v1.PostSearchReq) (ress *v1.PostSearchRes, errs error)
		// 新增岗位
		Add(ctx context.Context, req *v1.PostAddReq) (err error)
		//删除岗位
		Delete(ctx context.Context, ids []int) (errs error)
		//编辑岗位
		Edit(ctx context.Context, req *v1.PostEditReq) (errs error)
		//获取单条岗位信息
		GetPostIds(ctx context.Context, userId uint64) (postIds []int64, err error)
		// GetUsedPost 获取正常状态的岗位
		GetUsedPost(ctx context.Context) (list []*model.SysPost, err error)
	}
)

var (
	localSysPost ISysPost
)

func SysPost() ISysPost {
	if localSysPost == nil {
		panic("implement not found for interface ISysPost, forgot register?")
	}
	return localSysPost
}

func RegisterSysPost(i ISysPost) {
	localSysPost = i
}
