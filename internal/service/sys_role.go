// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model"
)

type (
	ISysRole interface {
		GetRoleListSearch(ctx context.Context, req *v1.RoleListReq) (res *v1.RoleListRes, err error)
		/*
			获取角色列表(这是给用户管理那里要调用的)
		*/
		GetRoleList(ctx context.Context) (list []*model.SysRole, errs error)
		// 给用户管理那边使用的
		AddUserRole(ctx context.Context, roleIds []int64, userId int64) (err error)
		// AddRoleRule 添加角色权限
		AddRoleRule(ctx context.Context, ruleIds []uint, roleId int64) (err error)
		// BindRoleRule 绑定角色权限
		BindRoleRule(ctx context.Context, ruleId interface{}, roleIds []uint) (err error)
		// DeleteByIds 删除角色
		DeleteByIds(ctx context.Context, ids []int64) (err error)
		// DelRoleRule 删除角色权限
		DelRoleRule(ctx context.Context, roleId int64) (err error)
		Get(ctx context.Context, id uint) (res *model.SysRole, err error)
		GetMenuRoles(ctx context.Context, id uint) (roleIds []uint, err error)
		// GetFilteredNamedPolicy 获取角色关联的菜单规则
		GetFilteredNamedPolicy(ctx context.Context, id uint) (gpSlice []int, err error)
		// EditRole 修改角色
		EditRole(ctx context.Context, req *v1.RoleEditReq) (err error)
		//添加角色,给角色管理使用的
		AddRole(ctx context.Context, req *v1.RoleAddReq) (err error)
	}
)

var (
	localSysRole ISysRole
)

func SysRole() ISysRole {
	if localSysRole == nil {
		panic("implement not found for interface ISysRole, forgot register?")
	}
	return localSysRole
}

func RegisterSysRole(i ISysRole) {
	localSysRole = i
}
