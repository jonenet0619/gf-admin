// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model"
)

type (
	ISysMenu interface {
		//获取菜单 这里是菜单管理使用的
		GetMenusAllMenu(ctx context.Context) (out []*model.SysMenuList, errs error)
		//获取菜单 这里是给角色管理里面设置权限使用的
		GetRoleAllMenu(ctx context.Context) (out []*model.SysMenuList, errs error)
		GetRoleMenuTree(menus []*model.SysMenuList, pid uint) []*model.SysMenuList
		//获取菜单,这里是给左侧菜单栏使用的
		GetAllMenuList(ctx context.Context) (out []*model.UserMenus, errs error)
		GetIsMenuList(ctx context.Context, isFlag bool) (out []*model.SysMenuList, errs error)
		// GetMenuList 获取所有菜单
		GetDbMenuList(ctx context.Context) (list []*model.SysMenuList, err error)
		GetMenusTree(menus []*model.UserMenus, pid uint) []*model.UserMenus
		SetMenuData(menu *model.UserMenu, entity *model.SysMenuList) *model.UserMenu
		GetIsButtonList(ctx context.Context) ([]*model.SysMenuList, error)
		Get(ctx context.Context, id uint) (rule *model.SysMenuList, err error)
		// Add 添加菜单
		Add(ctx context.Context, req *v1.RuleAddReq) (err error)
		// DeleteMenuByIds 删除菜单
		DeleteMenuByIds(ctx context.Context, ids []int) (err error)
		FindSonByParentId(list []*model.SysMenuList, pid uint) []*model.SysMenuList
		Update(ctx context.Context, req *v1.RuleUpdateReq) (err error)
	}
)

var (
	localSysMenu ISysMenu
)

func SysMenu() ISysMenu {
	if localSysMenu == nil {
		panic("implement not found for interface ISysMenu, forgot register?")
	}
	return localSysMenu
}

func RegisterSysMenu(i ISysMenu) {
	localSysMenu = i
}
