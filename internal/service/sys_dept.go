// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model"
)

type (
	ISysDept interface {
		//获取部门数据
		GetList(ctx context.Context, req *v1.DeptSearchReq) (list []*model.SysDept, errs error)
		//处理部门树形结构
		GetDeptTree(depts []*model.SysDept, pid int64) []*model.SysDept
		//删除部门数据
		DeleteDept(ctx context.Context, id int64) error
		//处理要删除的树结构数据
		FindSonByParentId(deptList []*model.SysDeptList, deptId int64) []*model.SysDeptList
		//编辑部门
		EditDept(ctx context.Context, req *v1.DeptEditReq) (errs error)
		//添加部门数据
		Add(ctx context.Context, req *v1.DeptAddReq) (errs error)
	}
)

var (
	localSysDept ISysDept
)

func SysDept() ISysDept {
	if localSysDept == nil {
		panic("implement not found for interface ISysDept, forgot register?")
	}
	return localSysDept
}

func RegisterSysDept(i ISysDept) {
	localSysDept = i
}
