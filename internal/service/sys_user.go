// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model"
	"gf-admin/internal/model/entity"

	"github.com/gogf/gf/v2/database/gdb"
)

type (
	ISysUser interface {
		GetCasBinUserPrefix() string
		GetAdminRules(ctx context.Context, userInfo *model.LoginUserRes) (sysMenuList []*model.UserMenus, permissions []string, err error)
		GetPermissions(ctx context.Context, roleIds []uint) (userButtons []string, err error)
		GetAdminMenusByRoleIds(ctx context.Context, roleIds []uint) (menus []*model.UserMenus, err error)
		//获取用户角色
		GetAdminRole(ctx context.Context, userId uint64, allRoleList []*model.SysRole) (roles []*model.SysRole, err error)
		//获取用户校色Ids
		GetAdminRoleIds(ctx context.Context, userId uint64) (roleIds []uint, errs error)
		FindUserNameInfo(ctx context.Context, userName string) (userInfo *entity.SysUser, errs error)
		FindUserIdInfo(ctx context.Context, userId int, token string) (mLoginUserRes *model.LoginUserRes, errs error)
		GetCaptcha() (out *model.UserCaptchaOutput, err error)
		// 获取用户列表
		List(ctx context.Context, req *v1.UserSearchReq) (total interface{}, userList []*model.SysUser, err error)
		//删除用户
		Delete(ctx context.Context, ids []int) (err error)
		// 获取编辑用户信息
		GetEditUser(ctx context.Context, id uint64) (res *v1.UserGetEditRes, err error)
		//修改用户信息
		Edit(ctx context.Context, req *v1.UserEditReq) (err error)
		//添加用户
		Add(ctx context.Context, req *v1.UserAddReq) (err error)
		// EditUserRole 修改用户角色信息
		EditUserRole(ctx context.Context, roleIds []int64, userId int64) (err error)
		// AddUserPost 添加用户岗位信息
		AddUserPost(ctx context.Context, tx gdb.TX, postIds []int64, userId int64) (err error)
		UserNameOrMobileExists(ctx context.Context, userName, mobile string, id ...int64) error
		//通过Id获取用户信息
		GetUserInfoById(ctx context.Context, id uint64, withPwd ...bool) (user *model.SysUser, err error)
		//获取多个用户角色,部门信息
		GetUsersRoleDept(ctx context.Context, userList []*model.SysUser) (userListRes []*model.SysUserRoleDeptRes, err error)
		// GetParams 获取岗位信息和角色信息
		GetParams(ctx context.Context, req *v1.UserGetParamsReq) (ress *v1.UserGetParamsRes, errs error)
		//重置用户密码
		// ResetUserPwd 重置用户密码
		ResetUserPwd(ctx context.Context, req *v1.UserResetPwdReq) (err error)
	}
)

var (
	localSysUser ISysUser
)

func SysUser() ISysUser {
	if localSysUser == nil {
		panic("implement not found for interface ISysUser, forgot register?")
	}
	return localSysUser
}

func RegisterSysUser(i ISysUser) {
	localSysUser = i
}
