// ================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// You can delete these comments if you wish manually maintain this interface file.
// ================================================================================

package service

import (
	"context"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/model/entity"
)

type (
	ISysDictType interface {
		// 获取 字典类型列表
		SearchList(ctx context.Context, req *v1.DictTypeSearchReq) (res *v1.DictTypeSearchRes, errs error)
		//添加新的字典
		Add(ctx context.Context, req *v1.DictTypeAddReq, userId uint64) (err error)
		// ExistsDictType 检查类型是否已经存在
		ExistsDictType(ctx context.Context, dictType string, dictId ...int64) (err error)
		//  删除字典类型
		Delete(ctx context.Context, dictIds []int) (errs error)
		// 获取单条字典数据
		Get(ctx context.Context, req *v1.DictTypeGetReq) (dictType *entity.SysDictType, errs error)
		//修改字典
		Edit(ctx context.Context, req *v1.DictTypeEditReq, userId uint64) (err error)
	}
)

var (
	localSysDictType ISysDictType
)

func SysDictType() ISysDictType {
	if localSysDictType == nil {
		panic("implement not found for interface ISysDictType, forgot register?")
	}
	return localSysDictType
}

func RegisterSysDictType(i ISysDictType) {
	localSysDictType = i
}
