package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserEdit(ctx context.Context, req *v1.UserEditReq) (res *v1.UserEditRes, errs error) {
	err := service.SysUser().Edit(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
