package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) PostSearch(ctx context.Context, req *v1.PostSearchReq) (res *v1.PostSearchRes, err error) {
	list, err := service.SysPost().List(ctx, req)
	if err != nil {
		return nil, err
	}
	return list, nil
}
