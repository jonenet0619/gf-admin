package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) PostEdit(ctx context.Context, req *v1.PostEditReq) (res *v1.PostEditRes, errs error) {
	err := service.SysPost().Edit(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
