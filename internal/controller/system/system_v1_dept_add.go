package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DeptAdd(ctx context.Context, req *v1.DeptAddReq) (res *v1.DeptAddRes, errs error) {

	err := service.SysDept().Add(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
