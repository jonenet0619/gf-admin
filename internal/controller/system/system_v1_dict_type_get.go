package system

import (
	"context"
	"gf-admin/api/system/v1"
	"gf-admin/internal/service"
)

func (c *ControllerV1) DictTypeGet(ctx context.Context, req *v1.DictTypeGetReq) (res *v1.DictTypeGetRes, err error) {
	item, err := service.SysDictType().Get(ctx, req)
	if err != nil {
		return nil, err
	}
	//res = new(v1.DictTypeGetRes)

	return &v1.DictTypeGetRes{Content: &v1.SysDictType{
		DictId:    item.DictId,
		DictName:  item.DictName,
		DictType:  item.DictType,
		Status:    item.Status,
		CreateBy:  item.CreateBy,
		UpdateBy:  item.UpdateBy,
		Remark:    item.Remark,
		CreatedAt: item.CreatedAt,
		UpdatedAt: item.UpdatedAt,
	}}, nil
}
