package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RuleAdd(ctx context.Context, req *v1.RuleAddReq) (res *v1.RuleAddRes, errs error) {
	err := service.SysMenu().Add(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
