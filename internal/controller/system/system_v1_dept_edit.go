package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DeptEdit(ctx context.Context, req *v1.DeptEditReq) (res *v1.DeptEditRes, errs error) {

	err := service.SysDept().EditDept(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
