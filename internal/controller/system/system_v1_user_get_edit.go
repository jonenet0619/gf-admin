package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserGetEdit(ctx context.Context, req *v1.UserGetEditReq) (res *v1.UserGetEditRes, errs error) {
	user, err := service.SysUser().GetEditUser(ctx, req.Id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
