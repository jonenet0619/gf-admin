package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RoleEdit(ctx context.Context, req *v1.RoleEditReq) (res *v1.RoleEditRes, errs error) {
	err := service.SysRole().EditRole(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
