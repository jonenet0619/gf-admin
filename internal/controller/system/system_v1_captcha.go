package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) Captcha(ctx context.Context, req *v1.CaptchaReq) (res *v1.CaptchaRes, err error) {

	data, err := service.SysUser().GetCaptcha()
	if err != nil {
		return nil, err
	}
	return &v1.CaptchaRes{
		Content: &v1.Captcha{
			Id:        data.Id,
			RequestId: data.RequestId,
			Data:      data.Data,
		},
	}, nil
}
