package system

import (
	"context"

	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) SysUser(ctx context.Context, req *v1.SysUserReq) (res *v1.SysUserRes, err error) {
	return nil, gerror.NewCode(gcode.CodeNotImplemented)
}
