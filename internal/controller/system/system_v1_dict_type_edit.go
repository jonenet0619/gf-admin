package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictTypeEdit(ctx context.Context, req *v1.DictTypeEditReq) (res *v1.DictTypeEditRes, err error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return nil, err
	}
	err = service.SysDictType().Edit(ctx, req, e.Id)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
