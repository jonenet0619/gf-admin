package system

import (
	"context"
	"gf-admin/api/system/v1"
	"gf-admin/internal/service"
)

func (c *ControllerV1) RoleGetParams(ctx context.Context, req *v1.RoleGetParamsReq) (res *v1.RoleGetParamsRes, errs error) {
	res = &v1.RoleGetParamsRes{}
	list, err := service.SysMenu().GetRoleAllMenu(ctx)
	if err != nil {
		return nil, err
	}
	res.Content = list

	return res, nil
}
