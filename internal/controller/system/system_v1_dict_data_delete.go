package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictDataDelete(ctx context.Context, req *v1.DictDataDeleteReq) (res *v1.DictDataDeleteRes, errs error) {
	err := service.SysDictData().Delete(ctx, req.Ids)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
