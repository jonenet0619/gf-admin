package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictDataAdd(ctx context.Context, req *v1.DictDataAddReq) (res *v1.DictDataAddRes, errs error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return nil, err
	}
	err2 := service.SysDictData().Add(ctx, req, e.Id)
	if err2 != nil {
		return nil, err2
	}
	return nil, nil
}
