package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RoleList(ctx context.Context, req *v1.RoleListReq) (res *v1.RoleListRes, errs error) {
	search, err := service.SysRole().GetRoleListSearch(ctx, req)
	if err != nil {
		return nil, err
	}
	return search, nil
}
