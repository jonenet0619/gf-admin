package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictDataSearch(ctx context.Context, req *v1.DictDataSearchReq) (res *v1.DictDataSearchRes, err error) {

	list, err := service.SysDictData().List(ctx, req)
	if err != nil {
		return nil, err
	}

	return list, nil
}
