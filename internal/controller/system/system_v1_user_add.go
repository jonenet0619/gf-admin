package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserAdd(ctx context.Context, req *v1.UserAddReq) (res *v1.UserAddRes, errs error) {
	err := service.SysUser().Add(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
