package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) PostAdd(ctx context.Context, req *v1.PostAddReq) (res *v1.PostAddRes, errs error) {
	err := service.SysPost().Add(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
