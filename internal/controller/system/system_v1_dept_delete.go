package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DeptDelete(ctx context.Context, req *v1.DeptDeleteReq) (res *v1.DeptDeleteRes, errs error) {

	err := service.SysDept().DeleteDept(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
