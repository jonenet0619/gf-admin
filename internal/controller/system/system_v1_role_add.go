package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RoleAdd(ctx context.Context, req *v1.RoleAddReq) (res *v1.RoleAddRes, errs error) {
	err := service.SysRole().AddRole(ctx, req)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
