package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictTypeDelete(ctx context.Context, req *v1.DictTypeDeleteReq) (res *v1.DictTypeDeleteRes, errs error) {

	err := service.SysDictType().Delete(ctx, req.DictIds)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
