package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) PostDelete(ctx context.Context, req *v1.PostDeleteReq) (res *v1.PostDeleteRes, errs error) {
	err := service.SysPost().Delete(ctx, req.Ids)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
