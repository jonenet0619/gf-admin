package system

import (
	"context"
	"gf-admin/internal/service"
	lib "gf-admin/utility/lib"
	"github.com/gogf/gf/v2/frame/g"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RoleGet(ctx context.Context, req *v1.RoleGetReq) (res *v1.RoleGetRes, err error) {
	g.Try(ctx, func(ctx context.Context) {
		res = &v1.RoleGetRes{Content: &v1.RoleGet{}}
		res.Content.Role, err = service.SysRole().Get(ctx, req.Id)
		lib.ErrIsNil(ctx, err, "")
		res.Content.MenuIds, err = service.SysRole().GetFilteredNamedPolicy(ctx, req.Id)
		lib.ErrIsNil(ctx, err, "")
	})

	return res, nil
}
