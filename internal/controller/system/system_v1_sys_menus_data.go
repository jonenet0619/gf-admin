package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) SysMenusData(ctx context.Context, req *v1.SysMenusDataReq) (res *v1.SysMenusDataRes, errs error) {
	menu, err := service.SysMenu().GetMenusAllMenu(ctx)
	if err != nil {
		return nil, err
	}

	return &v1.SysMenusDataRes{Content: menu}, nil
}
