package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) GetDict(ctx context.Context, req *v1.GetDictReq) (res *v1.GetDictRes, errs error) {
	byType, err := service.SysDictData().GetDictWithDataByType(ctx, req)
	if err != nil {
		return nil, err
	}

	return byType, nil
}
