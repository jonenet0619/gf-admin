package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RuleUpdate(ctx context.Context, req *v1.RuleUpdateReq) (res *v1.RuleUpdateRes, errs error) {
	err := service.SysMenu().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
