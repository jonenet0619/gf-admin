package system

import (
	"context"
	"gf-admin/api/system/v1"
	"gf-admin/internal/service"
)

func (c *ControllerV1) DictTypeSearch(ctx context.Context, req *v1.DictTypeSearchReq) (res *v1.DictTypeSearchRes, err error) {

	/*	service.SysDictType().List(ctx,&v1.DictTypeSearchReq{
		DictName: req.DictName,
		DictType: req.DictType,
		Status:   req.Status,
		PageReq:  req.PageReq,
	})*/
	list, err := service.SysDictType().SearchList(ctx, req)
	if err != nil {
		return nil, err
	}

	return list, nil
}
