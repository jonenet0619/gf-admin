package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserDeptTreeSelect(ctx context.Context, req *v1.UserDeptTreeSelectReq) (res *v1.UserDeptTreeSelectRes, errs error) {

	list, err := service.SysDept().GetList(ctx, &v1.DeptSearchReq{
		Status: "1",
	})
	if err != nil {
		return nil, err
	}

	return &v1.UserDeptTreeSelectRes{Content: list}, nil
}
