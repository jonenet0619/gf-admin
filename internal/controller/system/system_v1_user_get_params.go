package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserGetParams(ctx context.Context, req *v1.UserGetParamsReq) (res *v1.UserGetParamsRes, errs error) {

	params, err := service.SysUser().GetParams(ctx, req)
	if err != nil {
		return nil, err
	}

	return params, nil
}
