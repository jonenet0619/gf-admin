package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RuleDelete(ctx context.Context, req *v1.RuleDeleteReq) (res *v1.RuleDeleteRes, errs error) {
	err := service.SysMenu().DeleteMenuByIds(ctx, req.Ids)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
