package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictDataGet(ctx context.Context, req *v1.DictDataGetReq) (res *v1.DictDataGetRes, errs error) {
	item, err := service.SysDictData().Get(ctx, req.DictCode)
	if err != nil {
		return nil, err
	}
	return item, nil
}
