package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictTypeAdd(ctx context.Context, req *v1.DictTypeAddReq) (res *v1.DictTypeAddRes, err error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return nil, err
	}
	errs := service.SysDictType().Add(ctx, req, e.Id)
	if errs != nil {
		return nil, errs
	}

	return nil, nil
}
