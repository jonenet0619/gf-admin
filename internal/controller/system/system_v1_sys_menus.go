package system

import (
	"context"
	"gf-admin/internal/service"
	"github.com/gogf/gf/v2/util/gconv"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) SysMenus(ctx context.Context, req *v1.SysMenusReq) (res *v1.SysMenusRes, err error) {

	err, m := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return nil, err
	}
	info, err := service.SysUser().FindUserIdInfo(ctx, gconv.Int(m.Id), "")
	if err != nil {
		return nil, err
	}
	menuList, permissions, err := service.SysUser().GetAdminRules(ctx, info)
	if err != nil {
		return nil, err
	}
	return &v1.SysMenusRes{Content: &v1.SysMenus{
		Permissions: permissions,
		MenuList:    menuList,
	}}, nil
}
