package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RoleDelete(ctx context.Context, req *v1.RoleDeleteReq) (res *v1.RoleDeleteRes, errs error) {
	err := service.SysRole().DeleteByIds(ctx, req.Ids)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
