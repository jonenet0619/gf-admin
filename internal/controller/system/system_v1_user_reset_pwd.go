package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserResetPwd(ctx context.Context, req *v1.UserResetPwdReq) (res *v1.UserResetPwdRes, errs error) {
	err := service.SysUser().ResetUserPwd(ctx, req)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
