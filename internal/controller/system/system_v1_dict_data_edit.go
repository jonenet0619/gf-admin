package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DictDataEdit(ctx context.Context, req *v1.DictDataEditReq) (res *v1.DictDataEditRes, errs error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return nil, err
	}

	err2 := service.SysDictData().Edit(ctx, req, e.Id)
	if err2 != nil {
		return nil, err2
	}
	return nil, nil
}
