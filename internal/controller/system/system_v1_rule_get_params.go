package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RuleGetParams(ctx context.Context, req *v1.RuleGetParamsReq) (res *v1.RuleGetParamsRes, err error) {
	res = &v1.RuleGetParamsRes{Content: &v1.RuleGetParams{}}
	res.Content.Roles, err = service.SysRole().GetRoleList(ctx)
	if err != nil {
		return nil, err
	}

	res.Content.Menus, err = service.SysMenu().GetRoleAllMenu(ctx)
	if err != nil {
		return nil, err
	}

	return res, nil
}
