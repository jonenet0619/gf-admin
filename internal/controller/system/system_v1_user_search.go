package system

import (
	"context"
	"gf-admin/api/system/v1"
	"gf-admin/internal/service"
)

func (c *ControllerV1) UserSearch(ctx context.Context, req *v1.UserSearchReq) (res *v1.UserSearchRes, errs error) {
	total, list, err := service.SysUser().List(ctx, req)
	if err != nil {
		return nil, err
	}

	///g.Dump(total, list)
	res = new(v1.UserSearchRes)
	dept, err2 := service.SysUser().GetUsersRoleDept(ctx, list)
	if err2 != nil {
		return nil, err2
	}
	res.Total = total
	res.Content = dept
	res.CurrentPage = req.PageNum
	return res, nil
}
