package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) UserDelete(ctx context.Context, req *v1.UserDeleteReq) (res *v1.UserDeleteRes, errs error) {
	err := service.SysUser().Delete(ctx, req.Ids)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
