package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) DeptSearch(ctx context.Context, req *v1.DeptSearchReq) (res *v1.DeptSearchRes, err error) {
	list, err := service.SysDept().GetList(ctx, req)
	if err != nil {
		return nil, err
	}

	return &v1.DeptSearchRes{Content: list}, nil
}
