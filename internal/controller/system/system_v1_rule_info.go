package system

import (
	"context"
	"gf-admin/internal/service"

	"gf-admin/api/system/v1"
)

func (c *ControllerV1) RuleInfo(ctx context.Context, req *v1.RuleInfoReq) (res *v1.RuleInfoRes, err error) {
	res = &v1.RuleInfoRes{Content: &v1.RuleInfo{}}
	res.Content.Rule, err = service.SysMenu().Get(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	res.Content.RoleIds, err = service.SysRole().GetMenuRoles(ctx, req.Id)
	if err != nil {
		return nil, err
	}
	return res, nil
}
