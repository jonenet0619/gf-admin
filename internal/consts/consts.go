package consts

const (
	ERROR             = -1
	ADMIN_PASSS_EMPTY = -2 //账号或密码不能为空
	ADMIN_PASSS_ERROR = -3 //查询登录账号密码数据库出错
)

const (
	GTokenAdminPrefix = "userId:"
	ContextKey        = "ContextKey" // 上下文变量存储键名，前后端系统共享
	USER_INFO         = "userId"
)

// Menu 菜单中的类型枚举值
const (
	// Directory 目录
	Directory uint = 0
	// Menu 菜单
	Menu uint = 1
	// Button 按钮
	Button uint = 2
)

const (
	PageSize = 10 //分页长度
)
