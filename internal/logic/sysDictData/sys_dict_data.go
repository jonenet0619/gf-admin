package sysDictData

import (
	"context"
	"errors"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/consts"
	"gf-admin/internal/dao"
	"gf-admin/internal/model"
	"gf-admin/internal/model/do"
	"gf-admin/internal/service"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

func init() {
	service.RegisterSysDictData(New())
}

func New() *sSysDictData {
	return &sSysDictData{}
}

type sSysDictData struct {
}

//获取字典数据列表
func (s *sSysDictData) List(ctx context.Context, req *v1.DictDataSearchReq) (res *v1.DictDataSearchRes, err error) {
	res = new(v1.DictDataSearchRes)
	m := dao.SysDictData.Ctx(ctx)
	if req != nil {
		if req.DictLabel != "" {
			m = m.Where(dao.SysDictData.Columns().DictLabel+" like ?", "%"+req.DictLabel+"%")
		}
		if req.Status != "" {
			m = m.Where(dao.SysDictData.Columns().Status+" = ", gconv.Int(req.Status))
		}
		if req.DictType != "" {
			m = m.Where(dao.SysDictData.Columns().DictType+" = ?", req.DictType)
		}
		res.Total, err = m.Count()
		if err != nil {
			return nil, errors.New("获取字典数据失败:" + err.Error())
		}
		if req.PageNum == 0 {
			req.PageNum = 1
		}
		res.CurrentPage = req.PageNum
	}
	if req.PageSize == 0 {
		req.PageSize = consts.PageSize
	}
	errs := m.Page(req.PageNum, req.PageSize).Order(dao.SysDictData.Columns().DictSort + " asc," +
		dao.SysDictData.Columns().DictCode + " asc").Scan(&res.Content)
	if errs != nil {
		return nil, errors.New("查询数据库获取字典数据失败:" + errs.Error())
	}
	return
}

//新增字典数据
func (s *sSysDictData) Add(ctx context.Context, req *v1.DictDataAddReq, userId uint64) (errs error) {
	_, err := dao.SysDictData.Ctx(ctx).Insert(do.SysDictData{
		DictSort:  req.DictSort,
		DictLabel: req.DictLabel,
		DictValue: req.DictValue,
		DictType:  req.DictType,
		CssClass:  req.CssClass,
		ListClass: req.ListClass,
		IsDefault: req.IsDefault,
		Status:    req.Status,
		CreateBy:  userId,
		Remark:    req.Remark,
	})
	if err != nil {
		return errors.New("添加字典数据失败:" + err.Error())
	}
	//清除缓存
	return
}

//删除字典数据
func (s *sSysDictData) Delete(ctx context.Context, ids []int) (errs error) {
	_, err := dao.SysDictData.Ctx(ctx).Where(dao.SysDictData.Columns().DictCode+" in(?)", ids).Delete()
	if err != nil {
		return errors.New("删除字典数据失败:" + err.Error())
	}

	return
}

//获取单条字典数据
func (s *sSysDictData) Get(ctx context.Context, dictCode uint) (res *v1.DictDataGetRes, errs error) {
	res = new(v1.DictDataGetRes)
	err := dao.SysDictData.Ctx(ctx).WherePri(dictCode).Scan(&res.Dict)
	if err != nil {
		return nil, errors.New("获取字典数据失败:" + err.Error())
	}
	return
}

//修改字典数据
func (s *sSysDictData) Edit(ctx context.Context, req *v1.DictDataEditReq, userId uint64) (errs error) {
	_, err := dao.SysDictData.Ctx(ctx).WherePri(req.DictCode).Update(do.SysDictData{
		DictSort:  req.DictSort,
		DictLabel: req.DictLabel,
		DictValue: req.DictValue,
		DictType:  req.DictType,
		CssClass:  req.CssClass,
		ListClass: req.ListClass,
		IsDefault: req.IsDefault,
		Status:    req.Status,
		UpdateBy:  userId,
		Remark:    req.Remark,
	})
	if errs != nil {
		return errors.New("修改字典数据失败:" + err.Error())
	}
	return
}

//这条接口就是给外部调用字典的
func (s *sSysDictData) GetDictWithDataByType(ctx context.Context, req *v1.GetDictReq) (dict *v1.GetDictRes,
	err error) {

	//从数据库获取
	dict = &v1.GetDictRes{
		Content: &v1.Dict{},
	}
	//获取类型数据
	err1 := dao.SysDictType.Ctx(ctx).Where(dao.SysDictType.Columns().DictType, req.DictType).
		Where(dao.SysDictType.Columns().Status, 1).Fields(model.DictTypeRes{}).Scan(&dict.Content.Info)
	if err1 != nil {
		return nil, errors.New("获取字典类型失败:" + err1.Error())
	}
	err2 := dao.SysDictData.Ctx(ctx).Fields(model.DictDataRes{}).
		Where(dao.SysDictData.Columns().DictType, req.DictType).
		Order(dao.SysDictData.Columns().DictSort + " asc," +
			dao.SysDictData.Columns().DictCode + " asc").
		Scan(&dict.Content.Values)
	if err2 != nil {
		return nil, errors.New("获取字典数据失败:" + err2.Error())
	}

	//设置给定的默认值
	for _, v := range dict.Content.Values {
		if req.DefaultValue != "" {
			if gstr.Equal(req.DefaultValue, v.DictValue) {
				v.IsDefault = 1
			} else {
				v.IsDefault = 0
			}
		}
	}
	return
}
