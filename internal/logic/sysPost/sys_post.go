package sysPost

import (
	"context"
	"errors"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/consts"
	"gf-admin/internal/dao"
	"gf-admin/internal/model"
	"gf-admin/internal/model/do"
	"gf-admin/internal/model/entity"
	"gf-admin/internal/service"
	liberr "gf-admin/utility/lib"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

func init() {
	service.RegisterSysPost(New())
}

func New() *sSysPost {
	return &sSysPost{}
}

type sSysPost struct {
}

// 岗位列表
func (s *sSysPost) List(ctx context.Context, req *v1.PostSearchReq) (ress *v1.PostSearchRes, errs error) {
	res := new(v1.PostSearchRes)
	m := dao.SysPost.Ctx(ctx)
	if req != nil {
		if req.PostCode != "" {
			m = m.Where("post_code like ?", "%"+req.PostCode+"%")
		}
		if req.PostName != "" {
			m = m.Where("post_name like ?", "%"+req.PostName+"%")
		}
		if req.Status != "" {
			m = m.Where("status", gconv.Uint(req.Status))
		}
	}
	total, err1 := m.Count()
	if err1 != nil {
		return nil, errors.New("获取岗位失败:" + err1.Error())
	}
	if req.PageNum == 0 {
		req.PageNum = 1
	}
	if req.PageSize == 0 {
		req.PageSize = consts.PageSize
	}
	res.Total = total
	res.CurrentPage = req.PageNum
	err := m.Page(req.PageNum, req.PageSize).Order("post_sort asc,post_id asc").Scan(&res.Content)
	if err != nil {
		return nil, errors.New("查询数据库岗位失败:" + err.Error())
	}
	return res, nil
}

// 新增岗位
func (s *sSysPost) Add(ctx context.Context, req *v1.PostAddReq) (err error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return err
	}
	_, err = dao.SysPost.Ctx(ctx).Insert(&do.SysPost{
		PostCode:  req.PostCode,
		PostName:  req.PostName,
		PostSort:  req.PostSort,
		Status:    req.Status,
		Remark:    req.Remark,
		CreatedBy: e.Id,
	})
	if err != nil {
		return errors.New("新增岗位失败:" + err.Error())
	}
	return
}

//删除岗位
func (s *sSysPost) Delete(ctx context.Context, ids []int) (errs error) {
	_, err := dao.SysPost.Ctx(ctx).Where(dao.SysPost.Columns().PostId+" in(?)", ids).Delete()
	if err != nil {
		return errors.New("删除失败:" + err.Error())
	}

	return nil
}

//编辑岗位
func (s *sSysPost) Edit(ctx context.Context, req *v1.PostEditReq) (errs error) {
	err, e := service.CtxInfo().GetUserInfo(ctx)
	if err != nil {
		return err
	}

	_, err1 := dao.SysPost.Ctx(ctx).WherePri(req.PostId).Update(do.SysPost{
		PostCode:  req.PostCode,
		PostName:  req.PostName,
		PostSort:  req.PostSort,
		Status:    req.Status,
		Remark:    req.Remark,
		UpdatedBy: e.Id,
	})
	if err1 != nil {
		return errors.New("修改岗位失败:" + err1.Error())
	}
	return
}

//获取单条岗位信息
func (s *sSysPost) GetPostIds(ctx context.Context, userId uint64) (postIds []int64, err error) {
	err = g.Try(ctx, func(ctx context.Context) {
		var list []*entity.SysUserPost
		err = dao.SysUserPost.Ctx(ctx).Where(dao.SysUserPost.Columns().UserId, userId).Scan(&list)
		liberr.ErrIsNil(ctx, err, "获取用户岗位信息失败")
		postIds = make([]int64, 0)
		for _, entity := range list {
			postIds = append(postIds, entity.PostId)
		}
	})
	return
}

// GetUsedPost 获取正常状态的岗位
func (s *sSysPost) GetUsedPost(ctx context.Context) (list []*model.SysPost, err error) {
	err = g.Try(ctx, func(ctx context.Context) {
		err = dao.SysPost.Ctx(ctx).Where(dao.SysPost.Columns().Status, 1).
			Order(dao.SysPost.Columns().PostSort + " ASC, " + dao.SysPost.Columns().PostId + " ASC ").Scan(&list)
		liberr.ErrIsNil(ctx, err, "获取岗位数据失败")
	})
	return
}
