package sysOperLog

import (
	"gf-admin/internal/service"
	"github.com/gogf/gf/v2/os/grpool"
	"net/http"
)

type sOperateLog struct {
	Pool *grpool.Pool
}

func New() *sOperateLog {
	return &sOperateLog{Pool: grpool.New(100)}
}
func init() {
	service.RegisterOperateLog(New())
}

func (s *sOperateLog) OperationLog(r *http.Request) {
	err, _ := service.CtxInfo().GetUserInfo(r.Context())
	if err != nil {
		//glog.Error(r.Context(),"上下文中没有获取到数据")
		return
	}

}
