// ==========================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// ==========================================================================

package logic

import (
	_ "gf-admin/internal/logic/casbin"
	_ "gf-admin/internal/logic/ctxInfo"
	_ "gf-admin/internal/logic/middleware"
	_ "gf-admin/internal/logic/sysDept"
	_ "gf-admin/internal/logic/sysDictData"
	_ "gf-admin/internal/logic/sysDictType"
	_ "gf-admin/internal/logic/sysMemu"
	_ "gf-admin/internal/logic/sysOperLog"
	_ "gf-admin/internal/logic/sysPost"
	_ "gf-admin/internal/logic/sysRole"
	_ "gf-admin/internal/logic/sysUser"
)
