package sysDept

import (
	"context"
	"errors"
	v1 "gf-admin/api/system/v1"
	"gf-admin/internal/dao"
	"gf-admin/internal/model"
	"gf-admin/internal/model/do"
	"gf-admin/internal/service"
)

func init() {
	service.RegisterSysDept(New())
}

func New() *sSysDept {
	return &sSysDept{}
}

type sSysDept struct {
}

//获取部门树状结构数据
func (s *sSysDept) GetList(ctx context.Context, req *v1.DeptSearchReq) (list []*model.SysDept, errs error) {
	var mList = make([]*model.SysDept, 0)
	m := dao.SysDept.Ctx(ctx)
	if req.DeptName != "" {
		m = m.Where(dao.SysDept.Columns().DeptName, req.DeptName)
	}
	if req.Status == "0" {
		m = m.Where(dao.SysDept.Columns().Status, req.Status)
	}
	if req.Status == "1" {
		m = m.Where(dao.SysDept.Columns().Status, req.Status)
	}
	err := m.Scan(&mList)
	if err != nil {
		return nil, errors.New("查询数据库获取部门数据失败:" + err.Error())
	}

	//判断下用户是否不是搜索过了,如果是的话就不去处理树结构了
	if req.DeptName != "" || req.Status == "0" {
		return mList, nil
	}
	if req.IsType == 1 {
		return mList, nil
	}

	treeList := s.GetDeptTree(mList, 0)

	return treeList, nil
}

//处理部门树形结构
func (s *sSysDept) GetDeptTree(depts []*model.SysDept, pid int64) []*model.SysDept {
	returnList := make([]*model.SysDept, 0, len(depts))
	for _, dept := range depts {
		if dept.ParentId == pid {
			mChildrenDept := s.GetDeptTree(depts, dept.DeptId)
			dept.Children = mChildrenDept
			returnList = append(returnList, dept)
		}
	}
	return returnList
}

//删除部门数据
func (s *sSysDept) DeleteDept(ctx context.Context, id int64) error {
	var deptList = make([]*model.SysDeptList, 0)
	err := dao.SysDept.Ctx(ctx).Scan(&deptList)
	if err != nil {
		return errors.New("查询数据库部门数据失败:" + err.Error())
	}
	children := s.FindSonByParentId(deptList, id)
	ids := make([]int64, 0)
	for _, v := range children {
		ids = append(ids, v.DeptId)
	}
	ids = append(ids, id)
	_, err2 := dao.SysDept.Ctx(ctx).WhereIn(dao.SysDept.Columns().DeptId, ids).Delete()
	if err2 != nil {
		return errors.New("数据库删除部门数据失败:" + err2.Error())
	}
	return nil
}

//处理要删除的树结构数据
func (s *sSysDept) FindSonByParentId(deptList []*model.SysDeptList, deptId int64) []*model.SysDeptList {
	children := make([]*model.SysDeptList, 0, len(deptList))
	for _, v := range deptList {
		if v.ParentId == deptId {
			children = append(children, v)
			fChildren := s.FindSonByParentId(deptList, v.DeptId)
			children = append(children, fChildren...)
		}
	}
	return children
}

//编辑部门
func (s *sSysDept) EditDept(ctx context.Context, req *v1.DeptEditReq) (errs error) {
	err1, m := service.CtxInfo().GetUserInfo(ctx)
	if err1 != nil {
		return err1
	}
	_, err := dao.SysDept.Ctx(ctx).WherePri(req.DeptId).Update(do.SysDept{
		ParentId:  req.ParentID,
		DeptName:  req.DeptName,
		OrderNum:  req.OrderNum,
		Leader:    req.Leader,
		Phone:     req.Phone,
		Email:     req.Email,
		Status:    req.Status,
		UpdatedBy: m.Id,
	})
	if err != nil {
		return errors.New("修改部门失败:" + err.Error())
	}
	return nil
}

//添加部门数据
func (s *sSysDept) Add(ctx context.Context, req *v1.DeptAddReq) (errs error) {
	err1, e := service.CtxInfo().GetUserInfo(ctx)
	if err1 != nil {
		return err1
	}

	_, err := dao.SysDept.Ctx(ctx).Insert(do.SysDept{
		ParentId:  req.ParentID,
		DeptName:  req.DeptName,
		OrderNum:  req.OrderNum,
		Leader:    req.Leader,
		Phone:     req.Phone,
		Email:     req.Email,
		Status:    req.Status,
		CreatedBy: e.Id,
	})
	if err != nil {
		return errors.New("添加部门失败:" + err.Error())
	}
	return nil
}
