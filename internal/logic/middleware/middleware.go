package middleware

import (
	"fmt"
	"gf-admin/internal/logic/casbin"
	"gf-admin/internal/model"
	"gf-admin/internal/service"
	"gf-admin/utility/response"
	"github.com/gogf/gf/v2/errors/gcode"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/gogf/gf/v2/util/gconv"
)

type sMiddleware struct {
}

func init() {
	service.RegisterMiddleware(New())
}

func New() *sMiddleware {
	return &sMiddleware{}
}

func (s *sMiddleware) CORS(r *ghttp.Request) {
	r.Response.CORSDefault()
	r.Middleware.Next()
}

// AuthCheckRole 权限检查中间件
/*func (s *sMiddleware) AuthCheckRole(r *ghttp.Request) {

	//var res, casbinExclude bool
	//var err error
	var casbinExclude bool

	err, info := service.CtxInfo().GetUserInfo(r.Context())
	if err != nil {
		response.JsonExits(r, 555, err.Error(), nil)
		return
	}

	if info.RoleKey == "admin" {
		r.Middleware.Next()
		return
	}

	for _, v := range utility.CasbinExclude {
		if utility.KeyMatch2(r.RequestURI, v.Url) && r.Method == v.Method {
			casbinExclude = true
			break
		}
	}
	if casbinExclude {
		g.Log().Infof(r.Context(), "Casbin exclusion, no validation method:%s path:%s", r.Method, r.RequestURI)
		r.Middleware.Next()
		return
	}

	response.JsonExits(r, 403, "对不起,您没有该接口的访问权限,请联系管理员", nil)
	return

}*/

// ResponseHandler 返回处理中间件
func (s *sMiddleware) ResponseHandler(r *ghttp.Request) {
	r.Middleware.Next()

	// 如果已经有返回内容，那么该中间件什么也不做
	if r.Response.BufferLength() > 0 {
		return
	}

	var (
		err             = r.GetError()
		res             = r.GetHandlerResponse()
		code gcode.Code = gcode.CodeOK
	)
	if err != nil {
		code = gerror.Code(err)
		if code == gcode.CodeNil {
			code = gcode.CodeInternalError
		}
		response.JsonExits(r, code.Code(), err.Error(), nil)

	} else {
		response.JsonExits(r, code.Code(), "ok", res)
	}
}

func (s *sMiddleware) AuthCasbin(r *ghttp.Request, userId uint64) {
	ctx := r.GetCtx()

	//err1, userInfo := service.CtxInfo().GetUserInfo(ctx)
	//if err1 != nil {
	//	response.JsonExits(r, -15, err1.Error(), g.Map{})
	//}

	accessParams := r.Get("accessParams").Strings()
	accessParamsStr := ""
	if len(accessParams) > 0 && accessParams[0] != "undefined" {
		accessParamsStr = "?" + gstr.Join(accessParams, "&")
	}
	url := gstr.TrimLeft(r.Request.URL.Path, "/") + accessParamsStr
	/*if r.Method != "GET" && adminId != 1 && url!="api/v1/system/login" {
		libResponse.FailJson(true, r, "对不起！演示系统，不能删改数据！")
	}*/
	//获取无需验证权限的用户

	if gconv.Int(userId) == 1 {
		r.Middleware.Next()
		//不要再往后面执行
		return
	}
	//获取地址对应的菜单id
	menuList, err := service.SysMenu().GetIsMenuList(ctx, false)
	if err != nil {
		g.Log().Error(ctx, err)
		response.JsonExits(r, -10, "请求数据失败", g.Map{})
	}
	var menu *model.SysMenuList
	for _, m := range menuList {
		ms := gstr.SubStr(m.Name, 0, gstr.Pos(m.Name, "?"))
		if m.Name == url || ms == url {
			menu = m
			break
		}
	}
	//只验证存在数据库中的规则
	if menu != nil {
		//若是不登录能访问的接口则不判断权限
		//excludePaths := g.Cfg().MustGet(ctx, "gfToken.excludePaths").Strings()
		excludePaths, errs := g.Cfg().Get(ctx, "gToken.ExcludePaths")
		if errs != nil {
			return
		}

		for _, p := range excludePaths.Strings() {
			if gstr.Equal(menu.Name, gstr.TrimLeft(p, "/")) {
				r.Middleware.Next()
				return
			}
		}
		//若存在不需要验证的条件则跳过
		if gstr.Equal(menu.Conditions, "nocheck") {
			r.Middleware.Next()
			return
		}
		menuId := menu.Id
		//菜单没存数据库不验证权限
		if menuId != 0 {
			//判断权限操作
			enforcer, err := casbin.CasbinEnforcer(ctx)
			if err != nil {
				g.Log().Error(ctx, err)
				response.JsonExits(r, -11, "获取权限失败", g.Map{})
			}
			hasAccess := false
			hasAccess, err = enforcer.Enforce(fmt.Sprintf("%s%d", service.SysUser().GetCasBinUserPrefix(), userId), gconv.String(menuId), "All")
			if err != nil {
				g.Log().Error(ctx, err)
				response.JsonExits(r, -12, "判断权限失败", g.Map{})

			}
			if !hasAccess {
				response.JsonExits(r, -13, "没有访问权限", g.Map{})

			}
		}
	} else if menu == nil && accessParamsStr != "" {
		response.JsonExits(r, -14, "您没有该接口的访问权限", g.Map{})
	}
	r.Middleware.Next()

}
