package ctxInfo

import (
	"context"
	"errors"
	"gf-admin/internal/consts"
	"gf-admin/internal/model/entity"
	"gf-admin/internal/service"
)

type sCtxInfo struct {
}

func init() {
	service.RegisterCtxInfo(New())
}

func New() *sCtxInfo {
	return &sCtxInfo{}
}

func (s *sCtxInfo) GetUserInfo(ctx context.Context) (err error, user *entity.SysUser) {
	info := ctx.Value(consts.USER_INFO)
	if info == nil {
		return errors.New("上下文中不存在用户信息，请检查是否登录"), nil
	}
	user, status := info.(*entity.SysUser)
	if status {
		return nil, user
	}
	return errors.New("上下文中断言失败"), nil
}
