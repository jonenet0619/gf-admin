package model

import "github.com/gogf/gf/v2/os/gtime"

type SysDept struct {
	DeptId    int64       `json:"deptId"    dc:"部门id"`
	ParentId  int64       `json:"parentId"  dc:"父部门id"`
	Ancestors string      `json:"ancestors" dc:"祖级列表"`
	DeptName  string      `json:"deptName"  dc:"部门名称"`
	OrderNum  int         `json:"orderNum"  dc:"显示顺序"`
	Leader    string      `json:"leader"    dc:"负责人"`
	Phone     string      `json:"phone"     dc:"联系电话"`
	Email     string      `json:"email"     dc:"邮箱"`
	Status    uint        `json:"status"    dc:"部门状态（0正常 1停用）"`
	CreatedBy uint64      `json:"createdBy" dc:"创建人"`
	UpdatedBy int64       `json:"updatedBy" dc:"修改人"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改时间"`
	DeletedAt *gtime.Time `json:"deletedAt" dc:"删除时间"`
	Children  []*SysDept  `json:"children"`
}
type SysDeptList struct {
	DeptId    int64       `json:"deptId"    dc:"部门id"`
	ParentId  int64       `json:"parentId"  dc:"父部门id"`
	Ancestors string      `json:"ancestors" dc:"祖级列表"`
	DeptName  string      `json:"deptName"  dc:"部门名称"`
	OrderNum  int         `json:"orderNum"  dc:"显示顺序"`
	Leader    string      `json:"leader"    dc:"负责人"`
	Phone     string      `json:"phone"     dc:"联系电话"`
	Email     string      `json:"email"     dc:"邮箱"`
	Status    uint        `json:"status"    dc:"部门状态（0正常 1停用）"`
	CreatedBy uint64      `json:"createdBy" dc:"创建人"`
	UpdatedBy int64       `json:"updatedBy" dc:"修改人"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改时间"`
	DeletedAt *gtime.Time `json:"deletedAt" dc:"删除时间"`
}
