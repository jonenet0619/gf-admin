package model

import "github.com/gogf/gf/v2/os/gtime"

type SysPost struct {
	PostId    uint64      `json:"postId"    dc:"岗位ID"`
	PostCode  string      `json:"postCode"  dc:"岗位编码"`
	PostName  string      `json:"postName"  dc:"岗位名称"`
	PostSort  int         `json:"postSort"  dc:"显示顺序"`
	Status    uint        `json:"status"    dc:"状态（0正常 1停用）"`
	Remark    string      `json:"remark"    dc:"备注"`
	CreatedBy uint64      `json:"createdBy" dc:"创建人"`
	UpdatedBy uint64      `json:"updatedBy" dc:"修改人"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"修改时间"`
	DeletedAt *gtime.Time `json:"deletedAt" dc:"删除时间"`
}
