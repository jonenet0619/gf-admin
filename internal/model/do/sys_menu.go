// =================================================================================
// Code generated and maintained by GoFrame CLI tool. DO NOT EDIT.
// =================================================================================

package do

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

// SysMenu is the golang structure of table sys_menu for DAO operations like Where/Data.
type SysMenu struct {
	g.Meta     `orm:"table:sys_menu, do:true"`
	Id         interface{} //
	Pid        interface{} // 父Id
	Name       interface{} // 规则名称
	Title      interface{} // 规则标题
	Icon       interface{} // 图标
	Conditions interface{} // 条件
	Remarks    interface{} // 备注
	MenuType   interface{} // 类型;0:目录,1:菜单,2:按钮
	Weigh      interface{} // 权重
	IsHide     interface{} // 显示状态
	Path       interface{} // 路由地址
	Component  interface{} // 组建路径
	IsLink     interface{} // 是否是外链接;1:是,2:否
	ModuleType interface{} // 所属模块
	ModelId    interface{} // 模型id
	IsIframe   interface{} // 是否内嵌iframe
	IsCached   interface{} // 是否缓存
	Redirect   interface{} // 路由重定向地址
	IsAffix    interface{} // 是否固定
	LinkUrl    interface{} // 链接地址
	CreatedAt  *gtime.Time // 创建日期
	UpdatedAt  *gtime.Time // 修改日期
}
