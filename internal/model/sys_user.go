package model

import (
	"github.com/gogf/gf/v2/os/gtime"
)

type SysUser struct {
	Id            uint64      `json:"id"            dc:""`
	UserName      string      `json:"userName"      dc:"用户名"`
	Mobile        string      `json:"mobile"        dc:"手机号码"`
	NickName      string      `json:"nickName"      dc:"用户昵称"`
	Birthday      int         `json:"birthday"      dc:"生日"`
	PassWord      string      `json:"passWord"      dc:"登录密码"`
	UserSalt      string      `json:"userSalt"      dc:"加盐"`
	UserStatus    uint        `json:"userStatus"    dc:"用户状态;0:禁用,1:正常,2:未验证"`
	UserEmail     string      `json:"userEmail"     dc:"用户邮箱"`
	Sex           int         `json:"sex"           dc:"性别:0保密,1:男,2:女"`
	Avatar        string      `json:"avatar"        dc:"用户头像"`
	DeptId        uint        `json:"deptId"        dc:"部门id"`
	Remarks       string      `json:"remarks"       dc:"备注"`
	IsAdmin       int         `json:"isAdmin"       dc:"是否是管理员;1:是 0:否"`
	Address       string      `json:"address"       dc:"联系地址"`
	Describes     string      `json:"describes"     dc:"描述信息"`
	LastLoginIp   string      `json:"lastLoginIp"   dc:"最后登录ip"`
	LastLoginTime *gtime.Time `json:"lastLoginTime" dc:"最后登录时间"`
	CreatedAt     *gtime.Time `json:"createdAt"     dc:"创建时间"`
	UpdatedAt     *gtime.Time `json:"updatedAt"     dc:"更新时间"`
	DeletedAt     *gtime.Time `json:"deletedAt"     dc:"删除时间"`
}

type UserInfo struct {
	Content *Content `json:"content"`
}

type Content struct {
	UserName    string `orm:"user_name,unique" json:"userName"`   // 用户名
	NickName    string `orm:"nick_name"   json:"nick_name" `      // 用户昵称
	UserSalt    string `orm:"user_salt"        json:"userSalt"`   // 加密盐
	UserStatus  uint   `orm:"user_status"      json:"userStatus"` // 用户状态;0:禁用,1:正常,2:未验证
	Avatar      string `orm:"avatar" json:"avatar"`               //头像
	DeptId      uint64 `orm:"dept_id"       json:"deptId"`        //部门id
	TokenType   string `json:"tokenType"`
	AccessToken string `json:"accessToken"`
}

type LoginUserRes struct {
	//Id          uint64 `orm:"id,primary"       json:"id"`         //
	UserName string `orm:"user_name,unique" json:"userName"` // 用户名
	NickName string `orm:"nick_name"   json:"nick_name" `    // 用户昵称
	//PassWord    string `orm:"pass_word"    json:"pass_word"`      // 登录密码;cmf_password加密
	UserSalt   string `orm:"user_salt"        json:"userSalt"`   // 加密盐
	UserStatus uint   `orm:"user_status"      json:"userStatus"` // 用户状态;0:禁用,1:正常,2:未验证
	//IsAdmin     int    `orm:"is_admin"         json:"isAdmin"`    // 是否后台管理员 1 是  0   否
	Avatar      string `orm:"avatar" json:"avatar"`        //头像
	DeptId      uint64 `orm:"dept_id"       json:"deptId"` //部门id
	TokenType   string `json:"tokenType"`
	AccessToken string `json:"accessToken"`
}

type UserCaptchaInput struct {
}

type UserCaptchaOutput struct {
	Id        string `json:"id"`
	RequestId string `json:"requestId"`
	Data      string `json:"data"`
}

// SysUserRoleDeptRes 带有部门、角色、岗位信息的用户数据
type SysUserRoleDeptRes struct {
	*SysUser
	Dept     *SysDept              `json:"dept"`
	RoleInfo []*SysUserRoleInfoRes `json:"roleInfo"`
	Post     []*SysUserPostInfoRes `json:"post"`
}

type SysUserRoleInfoRes struct {
	RoleId uint   `json:"roleId"`
	Name   string `json:"name"`
}

type SysUserPostInfoRes struct {
	PostId   int64  `json:"postId"`
	PostName string `json:"postName"`
}
