package model

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
)

type SysMenuList struct {
	g.Meta     `orm:"table:sys_menu"`
	Icon       string `json:"icon"       description:"图标"`
	IsAffix    uint   `json:"isAffix"    description:"是否固定"`
	IsHide     uint   `json:"isHide"     description:"显示状态"`
	IsIframe   uint   `json:"isIframe"   description:"是否内嵌iframe"`
	IsLink     uint   `json:"isLink"     description:"是否是外链接;1:是,2:否"`
	Title      string `json:"title"      description:"规则标题"`
	Conditions string `json:"conditions" description:"条件"`
	Remarks    string `json:"remarks"    description:"备注"`
	MenuType   uint   `json:"menuType"   description:"类型;0:目录,1:菜单,2:按钮"`
	Weigh      int    `json:"weigh"      description:"权重"`

	ModuleType string `json:"moduleType" description:"所属模块"`
	ModelId    uint   `json:"modelId"    description:"模型id"`

	IsCached uint   `json:"isCached"   description:"是否缓存"`
	Redirect string `json:"redirect"   description:"路由重定向地址"`

	LinkUrl   string         `json:"linkUrl"    description:"链接地址"`
	CreatedAt *gtime.Time    `json:"createdAt"  description:"创建日期"`
	UpdatedAt *gtime.Time    `json:"updatedAt"  description:"修改日期"`
	Children  []*SysMenuList `json:"children,omitempty"`
	Component string         `json:"component"  description:"组建路径"`
	Id        uint           `json:"id"         description:""`
	Name      string         `json:"name"       description:"规则名称"`
	Path      string         `json:"path"       description:"路由地址"`
	Pid       uint           `json:"pid"        description:"父Id"`
}

type SysMenu struct {
	g.Meta   `orm:"table:sys_menu"`
	MenuList []*UserMenus `json:"menuList"`
}

type UserMenus struct {
	*UserMenu `json:""`
	Children  []*UserMenus `json:"children"`
}

type UserMenu struct {
	Id        uint   `json:"id"`
	Pid       uint   `json:"pid"`
	Name      string `json:"name"`
	Component string `json:"component"`
	Path      string `json:"path"`
	*MenuMeta `json:"meta"`
}

type MenuMeta struct {
	Icon        string `json:"icon"`
	Title       string `json:"title"`
	IsLink      string `json:"isLink"`
	IsHide      bool   `json:"isHide"`
	IsKeepAlive bool   `json:"isKeepAlive"`
	IsAffix     bool   `json:"isAffix"`
	IsIframe    bool   `json:"isIframe"`
}
