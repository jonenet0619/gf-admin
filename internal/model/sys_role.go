package model

import "github.com/gogf/gf/v2/os/gtime"

type SysRole struct {
	Id        uint        `json:"id"        dc:""`
	Status    uint        `json:"status"    dc:"状态;0:禁用;1:正常"`
	ListOrder uint        `json:"listOrder" dc:"排序"`
	Name      string      `json:"name"      dc:"角色名称"`
	Remark    string      `json:"remark"    dc:"备注"`
	DataScope uint        `json:"dataScope" dc:"数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）"`
	CreatedAt *gtime.Time `json:"createdAt" dc:"创建时间"`
	UpdatedAt *gtime.Time `json:"updatedAt" dc:"更新时间"`
}
